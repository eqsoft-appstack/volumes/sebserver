const   fs              = require('fs-extra'),
        path            = require('node:path'),
        express         = require('express'),
        auth            = require('http-auth'),
        authConnect 	= require("http-auth-connect"),
        basic           = auth.basic({
                realm: "Basic Area",
                file: __dirname + "/users"
        })

const   monitorPort = (process.env.SEBSERVER_MONITOR_PORT) ? process.env.SEBSERVER_MONITOR_PORT : 8441,
        socketPort = (process.env.SEBSERVER_SOCKET_PORT) ? process.env.SEBSERVER_SOCKET_PORT : 8442


var conf = function conf() {
        if(conf.caller != conf.getInstance){
                throw new Error("This object cannot be instantiated")
        }

        this.ipAddress = (process.env.SEBSERVER_IP_ADDRESS) ? process.env.SEBSERVER_IP_ADDRESS : '127.0.0.1'
        this.monitorPort = monitorPort
        this.socketPort = socketPort
        this.auth = auth
        this.authConnect = authConnect
        this.basic = basic

        this.getSSLOptions = function() {
                var options =   {
                                key:    fs.readFileSync(__dirname + '/ssl/server.key'),
                                cert:   fs.readFileSync(__dirname + '/ssl/server.crt'),
                                ca:     [
                                                fs.readFileSync(__dirname + '/ssl/root-ca.crt'),
                                                fs.readFileSync(__dirname + '/ssl/signing-ca.crt')
                                        ],
                                        requestCert:        false,      // client cert is not required
                                        rejectUnauthorized: false       // reject invalid client certs
                                }
                return options
        }

        this.getApp = function() {
                var app = express()
                //var user = "";
                //var password = "";
                app.use('/mon',authConnect(basic))
                app.use(function (req, res, next) {
                        var filename = path.basename(req.url)
                        if (filename.startsWith('monitor.html')) {
                                //res.set('Set-Cookie', `sebmonitorauth={$req.headers.authorization};Path=/`)
                                res.set('Set-Cookie', "sebmonitorauth="+req.headers.authorization+";Path=/")
                        }
                        next()
                    })
                app.use('/mon',express.static(path.join(__dirname, 'mon')))
                /*
                app.use('/mon',
                        express.static(path.join(__dirname, 'mon'),
                        {
                                setHeaders: function (res, path, stat) {
                                        res.set('Set-Cookie', "myCookie=cookieValue;Path=/")
                                }
                        }
                ))
                */
                return app;
        }
}

conf.instance = null;

/**
 * Singleton getInstance definition
 * @return singleton class
 */
conf.getInstance = function(){
        if(this.instance === null){
                this.instance = new conf();
        }
        return this.instance;
}

module.exports = conf.getInstance();