//const http = require('node:http')
const https 			= require('node:https'),
      conf 				= require('./conf.js'),
      monitor			= require('./monitor.js'),
	  WebSocketServer 	= require('ws').Server,
	  utils				= require('./utils.js'),
	  out				= utils.out,
	  inspect			= utils.inspect

var socketServer = https.createServer(conf.getSSLOptions(), conf.getApp())
var wss = new WebSocketServer({
	clientTracking: true,
	server: socketServer 
})
monitor.init(wss)
wss.on('connection',function(socket, req) {
	out('server: connection')
	//inspect(req.headers)
	monitor.on_seb_connection(socket, req)
	socket.on('open', function() {
		out('socket open')
		//monitor.on_seb_open(socket, req)
	})
	//socket.on('open',monitor.on_seb_open)
	//socket.on('close',on_close)
	socket.on('close', function(code, reason) {
		out('socket close')
		monitor.on_seb_close(code, reason, req)
	})
	socket.on('message', function(data, isBinary) {
		out('socket message')
		monitor.on_seb_message(data, isBinary, req)
	})
	socket.on('error',function(error) {
		out('socket error')
		inspect(error)
		//monitor.on_seb_message(data, isBinary, req)
	})
})
wss.on('error', function(error) {
	out("server: error")
	inspect(error)
})

socketServer.listen(conf.socketPort,conf.ipAddress)
out('Websocket started on port ' + conf.socketPort)

