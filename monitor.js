var fs 				= require('fs-extra'),
	util 			= require('util'),
	utils			= require('./utils.js'),
	out				= utils.out,
	inspect         = utils.inspect,
	crypt			= require('crypto'),
	https 			= require('https'),
	WebSocketServer = require('ws').Server,
	conf			= require('./conf.js'),
	sebs			= {}, // public sebs with radmon key for table gui
	_sebs			= {}, // internal sebs with socket object
	sebmap			= {}, // mapping from wskey to key
	adminsExist	= false,
	handler		= {
				"shutdown":shutdown,
				"shutdownAll":shutdownAll,
				"reboot":reboot,
				"rebootAll":rebootAll,
				"restart": restart,
				"restartAll": restartAll,
				"showPassword":showPassword,
				"showPasswordAll":showPasswordAll,
				"hidePassword":hidePassword,
				"hidePasswordAll":hidePasswordAll
			}
var monitorServer = https.createServer(conf.getSSLOptions(), conf.getApp())
var wss = new WebSocketServer({
	clientTracking: true,
	server: monitorServer 
})
monitorServer.listen(conf.monitorPort,conf.ipAddress)
wss.on('connection', function(socket, req) {
		//inspect(socket)
	//inspect(req)
	//console.log(socket.CONNECTING)
	//console.log(socket.CLOSED)

	out("monitor: admin connected")
	adminsExist = true
	addData(socket)
	socket.on('open',on_open)
	socket.on('close',on_close)
	socket.on('message',on_message)
	socket.on('error',on_error)
})

out('Websocket for monitoring started on port ' + conf.monitorPort)

//var _checkSebConnections = setInterval(checkSebConnections,10000)

function checkSebConnections() {
	// if no admins connected return
	if (!adminsExist) { return }
	// if no _sebs sockets return
	//console.log("checkSebConnections")
	for (var k in _sebs) {
		var sock = _sebs[k].socket
		switch (sock.readyState) { // does not make sense, i have to check the socket connection with ping/pong heartbeat
			case sock.CLOSED :
				// check readystate of socket on hardkilled OS
				// broadcast dead socket to admin clients?
				out("seb zombie!")
			break
		}
		// check socket connections
	}
}


function on_open() {
	out("admin: on_open")
}

function on_close(code, message) {
	out("admin: on_close")
	adminsExist = (wss.clients.length > 0)
}

function on_message(data, isBinary) {
	out("admin: on_message: " + data + " isBinary: " + JSON.stringify(isBinary))
	var obj = JSON.parse(data)
	var h = handler[obj.handler]
	if (typeof h === 'function') {
		h.apply(undefined, [obj.opts, data])
	}
}

function on_error(error) {
	out("admin: on_error: " + error)
}

function addData(socket) {
	socket.send(JSON.stringify({"handler":"addData","opts":sebs}), { binary: false })
}

/* seb clients */
function on_seb_connection(socket, req, server) {
	out("monitor: seb connected")
	addSeb(socket, req)
}

function on_seb_connection_error(error, server) {
	out("monitor: seb connection error")
}

function on_seb_open(socket) {
	/*
	 * http://django-websocket-redis.readthedocs.org/en/latest/heartbeats.html
	var heartbeat_msg = '--heartbeat--', heartbeat_interval = null, missed_heartbeats = 0
	if (heartbeat_interval === null) {
        missed_heartbeats = 0
        heartbeat_interval = setInterval(function() {
            try {
                missed_heartbeats++
                if (missed_heartbeats >= 3)
                    throw new Error("Too many missed heartbeats.")
                ws.send(heartbeat_msg)
            } catch(e) {
                clearInterval(heartbeat_interval)
                heartbeat_interval = null
                console.warn("Closing connection. Reason: " + e.message)
                ws.close()
            }
        }, 5000)
    }
    */
	out("monitor: seb socket open")
}

function on_seb_close(code, message, req, socket) {
	out("monitor: seb socket closed")
	removeSeb(socket, req)
}

function on_seb_error(error, socket) {
	out("monitor: seb socket error")
}

function on_seb_message(data, isBinary, req, socket) {
	out("monitor: seb socket message")
	//var wskey = socket.upgradeReq.headers['sec-websocket-key']
	var wskey = req.headers['sec-websocket-key']
	var id = sebmap[wskey]
	var locked = false
	var pwdShown = false
	var sebdata = JSON.parse(data)
	switch (sebdata.handler) {
		case "locked" :
			locked = true
			break
		case "unlocked" : 
			locked = false
			break
		case "pwdShown" :
			pwdShown = true
			break
		case "pwdHidden" :
			pwdShown = false
			break
		default:
			locked = false
	}
	sebs[id]["locked"] = locked
	sebs[id]["pwdShown"] = pwdShown
	var seb = sebs[id]
	// add seb specific data for broadcasting
	var opts = sebdata.opts
	opts["seb"] = seb
	var obj = { "handler" : sebdata.handler+"Seb", "opts":opts }
	out(JSON.stringify(obj))
	broadcast( obj )
}

function broadcast(data) { // to all connected admin clients
	wss.clients.forEach((client) => {
		out("broadcast")
		client.send(JSON.stringify(data), { binary: false })
	})
}

function addSeb(socket, req, data) {
	//var ip = socket.upgradeReq.connection.remoteAddress.replace(/[f\:]/g,"")
	var ip = req.socket.remoteAddress.replace(/[f\:]/g,"")
	out(`ip: ${ip}`)
	var id = crypt.randomBytes(16).toString('hex')
	out(`id: ${id}`)
	//var wskey = socket.upgradeReq.headers['sec-websocket-key']
	//inspect(req.socket)
	var wskey = req.headers['sec-websocket-key']
	out(`wskey: ${wskey}`)
	//var wskey = req.rawHeaders['Sec-WebSocket-Key']
	var seb = {"id":id,"ip":ip,"locked":false}
	for (var k in sebmap) {
		mappedId = sebmap[k]
		if (sebs[mappedId].ip == seb.ip) {
			out(`seb with ip already exists ${seb.ip} : ${k}`)
			out(`deleting old seb`)
			broadcast( { "handler" : "removeSeb", "opts" : sebs[mappedId] } )
			try {
				_sebs[mappedId].socket.close()
			}
			catch(e) {
				out(e)
			}
			delete _sebs[mappedId]
			delete sebs[mappedId]
			delete sebmap[k]
		}
	}
	sebs[id] = seb
	_sebs[id] = {
		"socket":socket
	}
	sebmap[wskey] = id
	broadcast( { "handler" : "addSeb", "opts" : seb } )
}

function removeSeb(socket, req, data) {
	//var wskey = socket.upgradeReq.headers['sec-websocket-key']
	var wskey = req.headers['sec-websocket-key']
	out(`wskey: ${wskey}`)
	var id = sebmap[wskey]
	out(`id: ${id}`)
	broadcast( { "handler" : "removeSeb", "opts" : sebs[id] } )
	delete sebs[id]
	delete sebmap[wskey]
	delete _sebs[id]
}

/* handler */
function shutdown(seb, data) {
	out("monitor: shutdown " + seb.id)
	var socket = _sebs[seb.id].socket
	try {
		socket.send(data, { binary: false }) // forward data (same handler and opts object expected on seb client)
	}
	catch(e) {
		out(e)
	}
	//out("monitor: socket " + socket.send)
}

function shutdownAll(seb,data) {
	out("monitor: shutdownAll " + JSON.stringify(seb.ids))
	for (var idx in seb.ids) {
		var id = seb.ids[idx]
		shutdown({"id":id},JSON.stringify({"handler":"shutdown","opts":{"id":id}}))
	}
}

function reboot(seb,data) {
        out("monitor: reboot " + seb.id)
        var socket = _sebs[seb.id].socket
        try {
                socket.send(data,{ binary: false }) // forward data (same handler and opts object expected on seb client)
        }
        catch(e) {
                out(e)
        }
        //out("monitor: socket " + socket.send)
}

function rebootAll(seb,data) {
        out("monitor: rebootAll " + JSON.stringify(seb.ids))
        for (var idx in seb.ids) {
			out("monitor: idx " + idx)
			var id = seb.ids[idx]
			var obj_id = {"id":id}
			var obj_opts = JSON.stringify({"handler":"reboot","opts":{"id":id}})
			setTimeout(reboot,(idx * 2000),obj_id,obj_opts)
        }
}

function restart(seb,data) {
	out("monitor: restart " + seb.id)
	var socket = _sebs[seb.id].socket
	try {
			socket.send(data,{ binary: false }) // forward data (same handler and opts object expected on seb client)
	}
	catch(e) {
			out(e)
	}
}

function restartAll(seb,data) {
	out("monitor: restartAll " + JSON.stringify(seb.ids))
	for (var idx in seb.ids) {
		out("monitor: idx " + idx)
		var id = seb.ids[idx]
		var obj_id = {"id":id}
		var obj_opts = JSON.stringify({"handler":"restart","opts":{"id":id}})
		restart(obj_id,obj_opts)
	}
}

function showPassword(seb,data) {
        out("monitor: showPassword " + seb.id)
        var socket = _sebs[seb.id].socket
        try {
                socket.send(data, { binary: false }) // forward data (same handler and opts object expected on seb client)
        }
        catch(e) {
                out(e)
        }
}

function showPasswordAll(seb,data) {
	out("monitor: showPasswordAll")
	let obj = JSON.parse(data)
        let password = obj.opts.password
        for (var idx in seb.ids) {
                var id = seb.ids[idx]
                showPassword({"id":id},JSON.stringify({"handler":"showPassword","opts":{"id":id,"password":password}})) 
        }
}

function hidePassword(seb,data) {
        out("monitor: hidePassword " + seb.id)
        var socket = _sebs[seb.id].socket
        try {
                socket.send(data, { binary: false }) // forward data (same handler and opts object expected on seb client)
        }
        catch(e) {
                out(e)
        }
}

function hidePasswordAll(seb,data) {
	out("monitor: hidePasswordAll")
        let obj = JSON.parse(data)
        for (var idx in seb.ids) {
                var id = seb.ids[idx]
                hidePassword({"id":id},JSON.stringify({"handler":"hidePassword","opts":{"id":id}}))
        }
}

// monitor
var monitor = function () {
	if(monitor.caller != monitor.getInstance) {
		throw new Error("This object cannot be instanciated")
	}
	this.wss = null
	this.init = function(websocketserver) {
		monitor.wss = websocketserver
	}
	this.on_seb_connection = function( socket, req ) { on_seb_connection( socket, req, this ) }
	this.on_seb_connection_error = function( error ) { on_seb_connection_error( error, this ) }
	this.on_seb_open = function() { on_seb_open( this ) } 
	this.on_seb_close = function(code, message, req) { on_seb_close(code, message, req, this) }
	this.on_seb_error = function(error) { on_seb_error(error, this) }
	this.on_seb_message = function(data, isBinary, req) { on_seb_message(data, isBinary, req, this) } }
monitor.instance = null

monitor.getInstance = function(){
	if(this.instance === null){
		this.instance = new monitor()
	}
	return this.instance
}

module.exports = monitor.getInstance()
